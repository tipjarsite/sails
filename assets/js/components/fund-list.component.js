/**
 * <fund-list>
 * -----------------------------------------------------------------------------
 * A list of funds (possibly filtered by region, tag, etc)
 *
 * @type {Component}
 * -----------------------------------------------------------------------------
 */

parasails.registerComponent('fundList', {
  //  ╔═╗╦═╗╔═╗╔═╗╔═╗
  //  ╠═╝╠╦╝║ ║╠═╝╚═╗
  //  ╩  ╩╚═╚═╝╩  ╚═╝
  props: [
    'areaTitle',
    'businessesTitle',
    'communityTitle',
    'generalTitle',
    'regionKey',
    'regionTitle',
    'tags',
  ],

  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: function (){
    return {
      communityFunds: [],
      generalFunds: [],
      regionFunds: [],
    };
  },

  //  ╦ ╦╔╦╗╔╦╗╦
  //  ╠═╣ ║ ║║║║
  //  ╩ ╩ ╩ ╩ ╩╩═╝
  template: `
    <div>
      <div class="fund-list community-funds" v-if="communityFunds && communityFunds.length > 0">
        <h4>{{ communityTitle }}</h4>
        <div>
          <ul class="nav flex-column">
            <li class="nav-item" v-for="fund in communityFunds">
              <a class="nav-link" :href="fund.url" target="_blank" v-html="fund.name">
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="fund-list region-funds" v-if="regionFunds && regionFunds.length > 0">
        <h4>{{ regionTitle }} {{ areaTitle }} {{ businessesTitle }}</h4>
        <div>
          <ul class="nav flex-column">
            <li class="nav-item" v-for="fund in regionFunds">
              <a class="nav-link" :href="fund.url" target="_blank" v-html="fund.name">
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="fund-list general-funds" v-if="generalFunds && generalFunds.length > 0">
        <h4>{{ generalTitle }}</h4>
        <div>
          <ul class="nav flex-column">
            <li class="nav-item" v-for="fund in generalFunds">
              <a class="nav-link" :href="fund.url" target="_blank" v-html="fund.name">
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  `,

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  mounted: async function(){
    const allFunds = await Cloud.activeFunds.with({ region: this.regionKey, tags: this.tags });
    this.communityFunds = allFunds[this.communityTitle];
    this.generalFunds = allFunds[this.generalTitle];
    this.regionFunds = allFunds[this.regionTitle];
  },
  // ^Note that there is no `beforeDestroy()` lifecycle callback in this
  // component. This is on purpose, since the timing vs. `leave()` gets tricky.

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    //…
  }
});

