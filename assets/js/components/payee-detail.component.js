/**
 * <payee-detail>
 * -----------------------------------------------------------------------------
 * A list of payees (possibly filtered by region, tag, etc)
 *
 * @type {Component}
 * -----------------------------------------------------------------------------
 */

parasails.registerComponent('payeeDetail', {
  //  ╔═╗╦═╗╔═╗╔═╗╔═╗
  //  ╠═╝╠╦╝║ ║╠═╝╚═╗
  //  ╩  ╩╚═╚═╝╩  ╚═╝
  props: [
    'websiteTitle',
    'payeeId',
  ],

  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: function (){
    return {
      payee: {}
    };
  },

  //  ╦ ╦╔╦╗╔╦╗╦
  //  ╠═╣ ║ ║║║║
  //  ╩ ╩ ╩ ╩ ╩╩═╝
  template: `
    <div class="payee-detail" v-if="payee">

      <h2>{{ payee.name }}</h2>

      <p v-if="payee.address"><strong>{{ payee.address }}</strong></p>

      <p v-if="payee.description"><em>{{ payee.description }}</em></p>

      <p v-if="payee.gofundme"><a :href="payee.gofundme" target="_blank">GoFundMe</a></p>

      <p v-if="payee.other"><a :href="payee.other" target="_blank">{{ payee.other }}</a></p>

      <p v-if="payee.paypal">PayPal: {{ payee.paypal }}</p>

      <p v-if="payee.venmo">Venmo: {{ payee.venmo }}</p>

      <p v-if="payee.website"><a :href="payee.website" target="_blank">{{ websiteTitle }}</a></p>

      <p v-if="payee.facebook"><a :href="payee.facebook" target="_blank">Facebook</a></p>

    </div>
  `,

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  mounted: async function(){
    this.payee = await Cloud.findOnePayee(this.payeeId);
  },
  // ^Note that there is no `beforeDestroy()` lifecycle callback in this
  // component. This is on purpose, since the timing vs. `leave()` gets tricky.

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    //…
  }
});

