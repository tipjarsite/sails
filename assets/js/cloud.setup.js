/**
 * cloud.setup.js
 *
 * Configuration for this Sails app's generated browser SDK ("Cloud").
 *
 * Above all, the purpose of this file is to provide endpoint definitions,
 * each of which corresponds with one particular route+action on the server.
 *
 * > This file was automatically generated.
 * > (To regenerate, run `sails run rebuild-cloud-sdk`)
 */

Cloud.setup({

  /* eslint-disable */
  methods: {"updatePassword":{"verb":"PUT","url":"/api/v1/account/update-password","args":["password"]},"login":{"verb":"PUT","url":"/api/v1/entrance/login","args":["emailAddress","password","rememberMe"]},"sendPasswordRecoveryEmail":{"verb":"POST","url":"/api/v1/entrance/send-password-recovery-email","args":["emailAddress"]},"updatePasswordAndLogin":{"verb":"POST","url":"/api/v1/entrance/update-password-and-login","args":["password","token"]},"deliverContactFormMessage":{"verb":"POST","url":"/api/v1/deliver-contact-form-message","args":["emailAddress","topic","fullName","message"]},"activeFunds":{"verb":"GET","url":"/api/v1/funds/:region?","args":["region"]},"activePayees":{"verb":"GET","url":"/api/v1/payees/:region","args":["region"]},"findOnePayee":{"verb":"GET","url":"/api/v1/payee/:id","args":["id"]},"activeRegions":{"verb":"GET","url":"/api/v1/regions","args":[]}}
  /* eslint-enable */

});
