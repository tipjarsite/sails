/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝
  'GET /':                            'list-regions-or-default',

  'GET /:region':                     'payee/view-payee-list',
  'GET /:region/contact':             'view-contact',
  'GET /:region/login':               'entrance/view-login',
  'GET /:region/businesses':          'payee/view-payee-list',
  'GET /:region/businesses/:payee':   'payee/view-payee-detail',
  'GET /:region/businesses/signup':   'payee/view-payee-signup-form',

  'GET /account':                     'account/view-account-overview',
  'GET /account/password':            'account/view-edit-password',

  'GET /email/confirm':               'entrance/confirm-email',
  'GET /email/confirmed':             'entrance/view-confirmed-email',

  'GET /faq':                         'view-faq',

  'GET /funds/suggest':               'fund/view-suggest-fund-form',

  'GET /password/forgot':             'entrance/view-forgot-password',
  'GET /password/new':                'entrance/view-new-password',

  'GET /regions':                     'region/view-region-list',

  'GET /tags/suggest':                'tag/view-suggest-tag-form',


  //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗   ┬   ╔╦╗╔═╗╦ ╦╔╗╔╦  ╔═╗╔═╗╔╦╗╔═╗
  //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗  ┌┼─   ║║║ ║║║║║║║║  ║ ║╠═╣ ║║╚═╗
  //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝  └┘   ═╩╝╚═╝╚╩╝╝╚╝╩═╝╚═╝╩ ╩═╩╝╚═╝
  '/logout':                  '/api/v1/account/logout',


  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝
  // …


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
  // from the Parasails library, or by using those method names as the `action` in <ajax-form>.
  '/api/v1/account/logout':                               'account/logout',
  'PUT    /api/v1/account/update-password':               { action: 'account/update-password' },
  'PUT    /api/v1/entrance/login':                        { action: 'entrance/login' },
  'POST   /api/v1/entrance/send-password-recovery-email': { action: 'entrance/send-password-recovery-email' },
  'POST   /api/v1/entrance/update-password-and-login':    { action: 'entrance/update-password-and-login' },
  'POST   /api/v1/deliver-contact-form-message':          { action: 'deliver-contact-form-message' },

  'POST   /api/v1/funds':                                 'fund.create',
  'GET    /api/v1/funds/all':                             'fund.find',
  'GET    /api/v1/funds/:region?':                        { action: 'public/active-funds' },
  'GET    /api/v1/fund/:id':                              'fund.findOne',
  'PATCH  /api/v1/fund/:id':                              'fund.update',
  'DELETE /api/v1/fund/:id':                              'fund.destroy',
  'POST   /api/v1/fund/suggest':                          'fund/process-fund-suggestion',

  'POST   /api/v1/payees':                                'payee.create',
  'GET    /api/v1/payees/all':                            'payee.find',
  'GET    /api/v1/payees/:region':                        { action: 'public/active-payees' },
  'GET    /api/v1/payee/:id':                             { action: 'payee/find-one-payee' },
  'PATCH  /api/v1/payee/:id':                             'payee.update',
  'DELETE /api/v1/payee/:id':                             'payee.destroy',
  'POST   /api/v1/payee/signup':                          'payee/process-payee-signup',

  'POST   /api/v1/regions':                               'region.create',
  'GET    /api/v1/regions/all':                           'region.find',
  'GET    /api/v1/regions':                               { action: 'public/active-regions' },
  'GET    /api/v1/region/:id':                            'region.findOne',
  'PATCH  /api/v1/region/:id':                            'region.update',
  'DELETE /api/v1/region/:id':                            'region.destroy',

  'POST   /api/v1/tags':                                  'tag.create',
  'GET    /api/v1/tags/all':                              'tag.find',
  'GET    /api/v1/tags':                                  'public/active-tags',
  'GET    /api/v1/tag/:id':                               'tag.findOne',
  'PATCH  /api/v1/tag/:id':                               'tag.update',
  'DELETE /api/v1/tag/:id':                               'tag.destroy',
  'POST   /api/v1/tag/suggest':                           'tag/process-tag-suggestion',

  'POST   /api/v1/users':                                 'user.create',
  'GET    /api/v1/users':                                 'user.find',
  'GET    /api/v1/user/:id':                              'user.findOne',
  'PATCH  /api/v1/user/:id':                              'user.update',
  'DELETE /api/v1/user/:id':                              'user.destroy',

};
