/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  '*': [ 'is-logged-in', 'set-region' ],

  // Bypass the `is-logged-in` policy for:
  'account/logout': true,
  'deliver-contact-form-message': true,
  'entrance/*': true,
  'fund/find-one-fund': true,
  'fund/process-fund-suggestion': true,
  'fund/view-suggest-fund-form': true,
  'list-regions-or-default': true,
  'payee/find-one-payee': true,
  'payee/process-payee-signup': true,
  'payee/view-payee-detail': true,
  'payee/view-payee-list': true,
  'payee/view-payee-signup-form': true,
  'public/active-funds': true,
  'public/active-payees': true,
  'public/active-regions': true,
  'public/active-tags': true,
  'region/find-one-region': true,
  'region/view-region-list': true,
  'tag/find-one-tag': true,
  'tag/process-tag-suggestion': true,
  'tag/view-suggest-tag-form': true,
  'view-contact': true,

};
