/**
 * set-region
 *
 * Set the region.
 */
module.exports = async function (req, res, proceed) {

  const regionKey = req.session.regionKey;
  if (regionKey) {
    sails.view.locals.region = await sails.helpers.regionForKey(regionKey);
  }
  return proceed();

};
