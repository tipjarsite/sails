/**
 * Region.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    key: {
      type: 'string',
      required: true,
      unique: true,
      example: 'mke',
      description: 'Identifier for the region to be used in URLs. Probably an airport code.',
    },

    name: {
      type: 'string',
      required: true,
      example: 'Milwaukee',
      description: 'The display name of the region.',
    },

    locality: {
      type: 'string',
      required: false,
      example: 'Wisconsin',
      description: 'State, province, etc.'
    },

    countryCode: {
      type: 'string',
      required: false,
      example: 'US',
      description: 'ISO-3166-1 alpha-2 code.'
    },

    carryOutUrl: {
      type: 'string',
      allowNull: true,
      example: 'https://example.com/get-carryout-in-my-city',
      description: 'Link to a page listing available takeout options.',
    },

    externalUrl: {
      type: 'string',
      allowNull: true,
      example: 'https://tipjar.site/mke',
      description: 'A different website to link and/or redirect for this region.',
    },

    giftCardSuffix: {
      type: 'string',
      allowNull: true,
      example: 'milwaukee-wi',
      description: 'Part of the URL that goes after https://supportlocal.usatoday.com/city/ for this region.',
    },

    serviceIndustryTipsKey: {
      type: 'string',
      allowNull: true,
      example: 'wi/milwaukee',
      description: 'Part of the URL that goes after https://serviceindustry.tips/en/ for this region.',
    },

    latitude: {
      type: 'number',
      example: 43.05,
      allowNull: true,
    },

    longitude: {
      type: 'number',
      example: -87.95,
      allowNull: true,
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    funds: {
      collection: 'fund',
      via: 'regions',
    },

    payees: {
      collection: 'payee',
      via: 'region',
    },

    users: {
      collection: 'user',
      via: 'region',
    },

  },

};
