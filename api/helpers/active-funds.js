module.exports = {


  friendlyName: 'Active funds',


  description: '',


  inputs: {

    region: {
      type: 'string',
      required: false,
    },

    tags: {
      type: 'string',
      required: false,
    },

    all: {
      type: 'boolean',
      defaultsTo: false,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {
    const fundParams = { sort: 'name' };
    fundParams.where = sails.config.custom.activeFilter;
    const funds = await Fund.find(fundParams).populate('regions').populate('tags');

    const regions = await Region.find();

    regions.out = {};

    regions.addFund = function(fund) {
      const community = sails.__('title.community');
      let region = sails.__('title.general');
      let added = false;

      // see if it is a community fund (i.e. one of the tag titles matches the title.community)
      fund.tags.forEach(function (tag) {
        if(tag.title === community) {
          region = community;
          added = true;
        }
      });

      // if not a community fund, see if it matches the region
      if (!added && inputs.region) {
        fund.regions.forEach(function (fundRegion) {
          if (fundRegion.key === inputs.region) {
            region = fundRegion.name;
            added = true;
          }
        });
      }

      // make an empty array if the key does not exist
      if (!regions.out[region]) {
        regions.out[region] = [];
      }

      // add the fund to the array
      regions.out[region].push(fund);
    }

    // get the region object with the specified key
    regions.forKey = function (key) {
      regions.forEach(function (region) {
        if (region.key === key) {
          return region;
        }
      });
      return null;
    }

    let tags = inputs.tags;
    if (tags) {
      tags = tags.split(',');
    }

    // if tags are provided, the fund must match at least one of them
    funds.forEach( function (fund) {
      let tagMatched = false;
      if (tags && tags.length > 0 && fund.tags.length > 0) {
        tags.forEach(function (filterTag) {
          fund.tags.forEach(function (fundTag) {
            if (filterTag === fundTag.name) {
              tagMatched = true;
            }
          });
        });
      }

      if (!tags || tagMatched) {
        let added = false;
        if (inputs.all || !inputs.region) {
          // if all is true or no region provided, include if regions collection is empty
          if (fund.regions.length === 0) {
            regions.addFund(fund);
            added = true;
          }
        }
        if (!added && inputs.region) {
          // if region is provided, include if the key matches a region
          if (fund.regions.length > 0) {
            fund.regions.forEach(function (region) {
              if (region.key === inputs.region) {
                regions.addFund(fund);
              }
            });
          }
        }
      }
    });

    return regions.out;
  }


};

