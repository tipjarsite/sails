module.exports = {


  friendlyName: 'Login page',


  description: 'Find the login page based on current region or go to the default',


  inputs: {
    req: {
      type: 'ref',
      required: true,
    },
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {

    return exits.success('/' + await sails.helpers.regionKey(inputs.req) + '/login');

  }


};
