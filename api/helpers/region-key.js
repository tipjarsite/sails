module.exports = {


  friendlyName: 'Region key',


  description: '',


  inputs: {

    req: {
      type: 'ref',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {

    // get the region from the session
    const regionKey = inputs.req.session.regionKey;

    // if none, get the default region from the custom config
    if (typeof regionKey === 'undefined') {
      const regionParam = inputs.req.param('region');
      if (typeof regionParam === 'undefined') {
        return sails.config.custom.defaultRegion;
      }

      return regionParam;
    }

    return regionKey

  }


};

