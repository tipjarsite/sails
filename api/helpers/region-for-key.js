module.exports = {


  friendlyName: 'Find a region by its key',


  description: 'Active region.',


  inputs: {

    key: {
      type: 'string',
      required: true,
    },

  },


  exits: {

  },


  fn: async function (inputs) {

    // All done.
    if (inputs.key) {
      return await Region.findOne({ key: inputs.key });
    }
    return null;

  }


};
