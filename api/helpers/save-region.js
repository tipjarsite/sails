module.exports = {


  friendlyName: 'Save region',


  description: '',


  inputs: {

    req: {
      type: 'ref',
      required: true,
    },

  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs) {

    const req = inputs.req;
    const regionKey = req.param('region');
    if (regionKey) {
      inputs.req.session.regionKey = regionKey;
    } else {
      if (typeof inputs.req.session.regionKey !== 'undefined') {
        delete inputs.req.session.regionKey;
      }
    }

    return regionKey;

  }


};

