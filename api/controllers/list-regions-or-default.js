module.exports = {


  friendlyName: 'List regions or default',


  description: 'If only one region exists, go directly to that region. Otherwise, go to the region list.',


  inputs: {

  },


  exits: {

    redirect: {
      responseType: 'redirect',
    },

  },


  fn: async function (inputs, exits) {

    const regions = await Region.find()
    if (regions.length === 1) {
      throw { redirect: '/' + regions[0].key }
    }
    throw { redirect: '/regions' }

  }


};
