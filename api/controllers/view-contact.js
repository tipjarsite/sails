module.exports = {


  friendlyName: 'View contact',


  description: 'Display "Contact" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/contact'
    }

  },


  fn: async function () {

    await sails.helpers.saveRegion(this.req);

    // Respond with view.
    return {};

  }


};
