module.exports = {


  friendlyName: 'View login',


  description: 'Display "Login" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/entrance/login',
    },

    redirect: {
      description: 'The requesting user is already logged in.',
      responseType: 'redirect'
    }

  },


  fn: async function () {

    const regionKey = await sails.helpers.regionKey(this.req);
    await sails.helpers.saveRegion(this.req);

    if (this.req.me) {
      throw {redirect: '/admin'};
    }

    return {};

  }


};
