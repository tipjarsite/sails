module.exports = {


  friendlyName: 'List active funds',


  description: 'Active fund.',


  inputs: {

    region: {
      type: 'string',
      required: false,
    },

  },


  exits: {

  },


  fn: async function (inputs) {

    // All done.
    return this.res.json(
      await sails.helpers.activeFunds(
        inputs.region,
        this.req.param('tags'),
        sails.config.custom.includeGlobalFunds
      )
    );

  }


};
