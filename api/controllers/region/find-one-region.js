module.exports = {


  friendlyName: 'Find one region',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound'
    },

  },


  fn: async function (inputs) {

    // All done.
    const region = await Region.findOne({id: inputs.id});
    if (region) {
      return region;
    }
    throw 'notFound';

  }


};
