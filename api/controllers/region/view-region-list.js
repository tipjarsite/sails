module.exports = {


  friendlyName: 'View region list',


  description: 'Display "Region list" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/region/region-list'
    }

  },


  fn: async function () {

    const regionKey = await sails.helpers.saveRegion(this.req);

    // Respond with view.
    return {
      regions: await sails.helpers.activeRegions(),
    };

  }


};
