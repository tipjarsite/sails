module.exports = {


  friendlyName: 'View suggest tag form',


  description: 'Display "Suggest tag form" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/tag/suggest-tag-form'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
