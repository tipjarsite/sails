module.exports = {


  friendlyName: 'View payee list',


  description: 'Display "Payee list" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/payee/payee-list'
    }

  },


  fn: async function () {

    const regionKey = await sails.helpers.saveRegion(this.req);
    const region = await sails.helpers.regionForKey(regionKey);

    // Respond with view.
    return { region };

  }


};
