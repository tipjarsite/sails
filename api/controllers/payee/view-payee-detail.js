module.exports = {


  friendlyName: 'View payee detail',


  description: 'Display "Payee detail" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/payee/payee-detail'
    }

  },


  fn: async function () {

    const regionKey = await sails.helpers.saveRegion(this.req);
    const region = await sails.helpers.regionForKey(regionKey);

    // Respond with view.
    return { region };

  }


};
