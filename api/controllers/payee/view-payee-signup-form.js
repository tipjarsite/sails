module.exports = {


  friendlyName: 'View payee signup form',


  description: 'Display "Payee signup form" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/payee/payee-signup-form'
    }

  },


  fn: async function () {

    await sails.helpers.saveRegion(this.req);

    // Respond with view.
    return {};

  }


};
