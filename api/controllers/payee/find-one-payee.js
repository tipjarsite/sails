module.exports = {


  friendlyName: 'Find one payee',


  description: '',


  inputs: {

    id: {
      type: 'string',
      required: true,
    },

  },


  exits: {

    notFound: {
      responseType: 'notFound'
    },

  },


  fn: async function (inputs) {

    // All done.
    const payee = await Payee.findOne({id: inputs.id});
    if (payee) {
      return payee;
    }
    throw 'notFound';

  }


};
